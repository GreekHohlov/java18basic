package week9.oop3.arraylistexample;

public class Car {
    private String model;
    private String year;

    public Car(String model, String year){
        this.model = model;
        this.year = year;
    }

    public String getModel(){
        return model;
    }
    public String getYear(){
        return year;
    }

    public void setModel(String model){
        this.model = model;
    }
    public void setYear(String year){
        this.year = year;
    }

//    public void test(MyInterface ){
//
//    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}

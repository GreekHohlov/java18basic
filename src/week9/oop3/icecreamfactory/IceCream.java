package week9.oop3.icecreamfactory;

public interface IceCream {
    void printIngredients();
}

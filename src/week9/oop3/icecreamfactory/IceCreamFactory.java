package week9.oop3.icecreamfactory;
/*
реализация паттерна Фабрика для нашего мороженого
 */

public class IceCreamFactory {
    private IceCreamFactory() {

    }

    public static IceCream getIceCream(IceCreamType type){
        IceCream iceCream = null;
        switch (type){
            case CHERRY -> iceCream = new CherryIceCream();
            case CHOCOLATE -> iceCream = new ChocolateIceCream();
            case VANILLA -> iceCream = new VanillaIceCream();
        }
        return iceCream;
    }
}

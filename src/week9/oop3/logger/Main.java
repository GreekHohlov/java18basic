package week9.oop3.logger;

public class Main {
    public static void main(String[] args) {
        ConsoleLogger consoleLogger = new ConsoleLogger();
        consoleLogger.log("Мы в Main");
        Logger txtLogger = new TxtFileLogger();
        Bulb bulb = new Bulb(txtLogger);

        bulb.turnOn();
        bulb.turnOff();
        bulb.isShining();

        Logger csvLogger = new CsvFileLogger("лог_лампочки");
        Bulb bulb1 = new Bulb(csvLogger);
        bulb1.turnOn();
        bulb1.turnOff();
        bulb1.isShining();
    }
}

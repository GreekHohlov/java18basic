package week8.oop2.Task3;
/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */

import java.lang.reflect.Array;
import java.util.Arrays;

public class SimpleArrayList {
    private int size;
    private int[] array;
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currIndex;

    public SimpleArrayList(){
        array = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currIndex = 0;
    }
    public SimpleArrayList(int size){
        array = new int[size];
        capacity = size;
        this.size = 0;
        currIndex = 0;
    }


    public void add(int elem){
        if (currIndex >= capacity){
            capacity = 2 * capacity;
            array = Arrays.copyOf(array, capacity);
        }
        array[currIndex] = elem;
        size++;
        currIndex++;
    }

    public void remove(){
        for (int i = 0; i < currIndex; i++) {
            array[i] = array[i + 1];
        }
        array[currIndex] = -1;
        currIndex--;
        size--;
    }

    //Достает элемент по индексу
    public int get(int idx){
        if (idx < 0 || idx >= size){
            System.out.println("Невозможно взять элемент по заданному индексу: " + idx);
            return -1;
        }
        else {
            return array[idx];
        }
    }

    public int size(){
        return size;
    }
}

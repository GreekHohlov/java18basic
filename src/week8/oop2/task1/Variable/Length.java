package week8.oop2.task1.Variable;

public class Length {
    static int sum(int... numbers){
        int sum = 0;

        for (int i = 0; i < numbers.length; i++){
            sum += numbers[i];
        }
        return sum;
    }

    static boolean findChar(Character ch, String... strings){
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].indexOf(ch) !=-1){
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(sum(1, 2, 3, 4, 6));
        System.out.println(findChar('a', "python", "java"));
        int a = 123;
        System.out.println("" + a);
        System.out.println(String.format("This is an integer: %d", a));
    }
}

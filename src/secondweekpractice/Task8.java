package secondweekpractice;
/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a

 */
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        System.out.println(s);

        if ('s' >= 65 || 's' <= 90) {
            s = s.toUpperCase();
        } else if ('s' >= 97 || 's' <= 122) {
            s = s.toLowerCase();
        } else {
            System.out.println("введены некорректные данные");
        }
        System.out.println(s);
    }
}
////97 122

//public class Task8 {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        String symbol = scanner.next();
//        char c = symbol.charAt(0);
//
//        if (c >= 'a' && c <= 'z'){
//            System.out.println((char) (c + ('A' - 'a')));
//        } else {
//            System.out.println((char) (c - ('A' - 'a')));
//        }
//    }
//}

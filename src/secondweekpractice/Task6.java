package secondweekpractice;
/*
Даны три числа a, b, c.
Найти сумму двух чисел больших из них.
Входные данные
21 0 8
Выходные данные
29

 */
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        if (a >= b && b > c){
            System.out.println(a + b);
        }
        else if (b >= a && c > a){
            System.out.println(b + c);
        }
        else {
            System.out.println(a + c);
        }
//        System.out.println(Math.max(Math.max(a + b, b + c), a + c));
    }
}

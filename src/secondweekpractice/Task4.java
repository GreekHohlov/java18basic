package secondweekpractice;

import java.util.Scanner;

/*
Считать данные из консоли о типе номера отеля.
 1 VIP, 2 Premium, 3 Standard
 Вывести цену номера VIP = 125, Premium = 110, Standard = 100

 */
public class Task4 {
    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 110;
    public static final int STANDART_PRICE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        if (n == 1){
//            System.out.println("VIP = " + VIP_PRICE);
//        }
//        else if (n == 2){
//            System.out.println("Premium = " + PREMIUM_PRICE);
//        }
//        else if (n == 3){
//            System.out.println("Standard = " + STANDART_PRICE);
//        }
//        else
//            System.out.println("Введите корректный тип");
        switch (n) {
            case 1:
                System.out.println("VIP = " + VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium = " + PREMIUM_PRICE);
                break;
            case 3:
                System.out.println("Standard = " + STANDART_PRICE);
                break;
            default:
                System.out.println("Введите корректный тип");
                break;
        }

    }
}

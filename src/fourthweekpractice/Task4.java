package fourthweekpractice;
/*
Начальный вклад в банке равен 1000.
Каждый месяц размер вклада увеличивается на P процентов от имеющейся суммы (0 < P < 25).
Найти через какое количество времени размер вклада будет больше 1100
и вывести  найденное количество месяцев и итоговой размер вклада.

15 ->
1
1150.0

3 ->
4
1125.50881


 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int p = input.nextInt();
        double amount = 1000;
        int month = 0;
        while (amount < 1100){
            amount += (int)(p / 100.0 * amount * 100) / 100.0;
            month++;
        }
        System.out.println(month);
        System.out.println(amount);
    }
}

package fourthweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
//        Scanner input = new Scanner(System.in);
//        int n = input.nextInt();
//        int sum = 0;
//        int nLenght = String.valueOf(n).length() - 1;
//        for (int i = nLenght; i >= 0; i--){
//            sum += n / (int)Math.pow(10, i);
//            n -= n / (int)Math.pow(10, i) * (int)Math.pow(10, i);
//        }
//        System.out.println(sum);
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Входные данные: n= " + n);
        System.out.println("Выходные данные: ");

        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            System.out.println("SUM in WHILE LOOP: " + sum);

            n = n / 10;
            System.out.println("N in WHILE LOOP: " + n);
        }
        System.out.println("Answer: " + sum);

        //через преобразование в строку:
        /*
        sum += Integer.parseInt(String.valueOf(num.charAt(i)));
         */

    }
}

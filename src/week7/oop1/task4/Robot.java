package week7.oop1.task4;
/*
Робот. Команды повернуть влево, повернуть вправо, идти на 1 шаг.
Несколько конструкторов, хранение координат, вывод потом координат на экран.
 */

public class Robot {
    private int x;
    private int y;
    private Direction direction; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left

    public Robot(){
        this.x = 0;
        this.y = 0;
        this.direction = Direction.UP;
    }
    public Robot(int x, int y){
        this.x = x;
        this.y = y;
        this.direction = Direction.UP;
    }

    public void go(){
        switch (direction){
//            case 0 -> y++;
//            case 1 -> x++;
//            case 2 -> y--;
//            case 3 -> x--;
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case BOTTOM:
                y--;
                break;
            case LEFT:
                x--;
                break;
        }
    }
    public void turnLeft(){
//        this.direction = (direction - 1) % 4;
        this.direction = Direction.ofNumber((this.direction.number + 3) % 4);
    }
    public void turnRight(){
//        this.direction = (direction + 1) % 4;
        this.direction = Direction.ofNumber((this.direction.number + 1) % 4);
    }
    public void printCoordinates(){
        System.out.println("(x,y) = " + x + ", " + y);
    }
}

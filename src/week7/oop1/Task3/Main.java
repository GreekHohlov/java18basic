package week7.oop1.Task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("greek-hohlov@yandex.ru"));
        System.out.println(FieldValidator.validateDate("10.08.1987"));
        System.out.println(FieldValidator.validateName("Boris"));
        System.out.println(FieldValidator.validatePhone("+79137842113"));
    }
}

package week7.oop1.task1;

public class Chandelier {
    private Bulb[] chandelier;

    public Chandelier(int countOfBulbs){
        chandelier = new Bulb[countOfBulbs];

        for (int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb();
        }
    }
    public void turnOn(){
        for (Bulb bulb : chandelier) {
            bulb.turnOn();
        }
    }
    public void turnOf(){
        for (Bulb bulb : chandelier) {
            bulb.turnOf();
        }
    }
    public boolean isShining(){
        return chandelier[0].isShining();
    }
}

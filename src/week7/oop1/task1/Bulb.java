package week7.oop1.task1;
/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
 */

public class Bulb {
    private boolean toggle;
    public Bulb(){
        this.toggle = false;
    }

    public Bulb(boolean condition){
        this.toggle = condition;
    }

//    public Bulb(boolean toogle){
//        this.toggle;
//    }

    public void turnOn(){
        this.toggle = true;
    }
    public void turnOf(){
        this.toggle = false;
    }

    public boolean isShining(){
        return this.toggle;
    }
}

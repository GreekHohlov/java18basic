package week7.oop1.task2;

public class Main {
    public static void main(String[] args) {
//        Thermometer thermometer = new Thermometer(-25, "C");
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.FAHRENHEIT);
        Thermometer thermometer1 = new Thermometer(-25, TemperatureUnit.CELSIUS);
        System.out.println("В цельсих: " + thermometer.getTempCelsius());
        System.out.println("В фаренгейтах: " + thermometer1.getTempFahrenheit());
    }

}

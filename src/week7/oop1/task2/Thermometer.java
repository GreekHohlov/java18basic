package week7.oop1.task2;
/*

Реализовать класс “Термометр”.
Необходимо иметь возможность создавать инстанс класса с текущей температурой и
получать значение в фаренгейте и в цельсии.
 */

public class Thermometer {
    private double tempCelsius;
    private double tempFahrenheit;

//    public Thermometer(double currentTemperature, String temperatureUnit){
//        if (temperatureUnit.equals("C")){
//            tempCelsius = currentTemperature;
//            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
//        } else if (temperatureUnit.equals("F")) {
//            tempFahrenheit = currentTemperature;
//            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
//        }
//        else {
//            System.out.println("Температура не распознана. Единицы измерения по умолчанию = цельсий");
//            tempFahrenheit = currentTemperature;
//            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
////            System.out.println("ERROR! ALARM!");
////            throw new UnsupportedOperationException("Температура не распознана");
//        }
//    }

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit){
        if (temperatureUnit == TemperatureUnit.CELSIUS){
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            tempFahrenheit = currentTemperature;
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
        }
        else {
            System.out.println("Температура не распознана. Единицы измерения по умолчанию = цельсий");
            tempFahrenheit = currentTemperature;
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
//            System.out.println("ERROR! ALARM!");
//            throw new UnsupportedOperationException("Температура не распознана");
        }
    }

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    private double fromCelsiusToFahrenheit(double currentTemperature){
        return currentTemperature * 1.8 + 32;
    }
    private double fromFahrenheitToCelsius(double currentTemperature){
        return (currentTemperature - 32) / 1.8;
    }
}

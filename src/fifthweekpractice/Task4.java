package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7

 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr1 = new int[n];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = scanner.nextInt();
        }
        int k = scanner.nextInt();
        int[] arr2 = new int[k];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = scanner.nextInt();
        }
        mergeTwoArraysWithLop(arr1, arr2);
        mergeToArraysWithSystemArrayCopy(arr1, arr2);
        mergeTwoArrays(arr1, arr2);
    }

    /**
     * Метод выполняет слияние двух массивов в третий, выполняет сортировку нового массива и выводит на печать
     * @param arr1 - первый объединяемый массив
     * @param arr2 - второй объединяемый массив
     */
    public static void mergeTwoArraysWithLop(int[] arr1, int[] arr2){
        int[] mergeArray = new int[arr1.length + arr2.length];
        int pos = 0;
        for (int element: arr1) {
            mergeArray[pos] = element;
            pos++;
        }
        for (int element: arr2) {
            mergeArray[pos] = element;
            pos++;
        }

        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }

    public static void mergeToArraysWithSystemArrayCopy(int[] arr1, int[] arr2){
        int[] mergeArray = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, mergeArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergeArray, arr1.length, arr2.length);
        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }

    public static void mergeTwoArrays(int[] arr1, int[] arr2){
        int[] mergeArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergeArray[k++] = arr1[i++];
            } else {
                mergeArray[k++] = arr2[j++];
            }
        }
            //сохраняем оставшиеся элементы первого массива
            while (i < arr1.length){
                mergeArray[k++] = arr1[i++];

            }
            //сохраняем оставшиеся элементы второго массива
            while (j < arr2.length){
                mergeArray[k++] = arr2[j++];
            }
            System.out.println(Arrays.toString(mergeArray));
    }
}

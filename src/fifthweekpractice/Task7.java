package fifthweekpractice;

import java.util.Scanner;
/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
5
Hello
good
to
see
you
     4

     Выходные данные:
     good to see you
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = 0;
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }
        int m = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            if (arr[i].length() <= m){
                k++;
            }
        }

        String[] arr2 = new String[k];
        k = 0;
        for (String s: arr) {
            if (s.length() <=m){
              arr2[k] = s;
              k++;
            }
        }
        for (String s: arr2) {
            System.out.print(s + " ");
        }
    }
}

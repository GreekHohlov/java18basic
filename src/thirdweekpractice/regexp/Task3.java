package thirdweekpractice.regexp;

import java.util.Scanner;

/*
Запросить у пользователя имя, день рождения, номер телефона, email.
Каждое из полученных ответов проверить регулягым выражением по описанным ниже правилам.
Если все введено верно, то вывести "Ок"
Если хотя бы одно из полей не соответствует - вывести "Wrong Answer" и завершить работу программы.
Проверки:
Имя - должно содержать только буквы. Начинаться с заглавной буквы и далее только прописные.
от 2 до 20 символов.

День рождения - должен иметь вид DD.MM.YYYY (DD, MM, YYYY - цифры без ограничений)

Номер телефона - должен начинаться со знака +, далее ровно 11 цифр

Email - вначале идут прописные буквы или один из спец. символов _ - * .
Далее обязательно символ @
Далее прописные буквы или цифры
Далее точка
Далее "com" или "ru"

Albert
13.12.1990
+79151112233
albert@mail.ru
Ok

albert
15.12.1990
+79151112233
albert@mail.ru
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String date = scanner.nextLine();
        String phone = scanner.nextLine();
        String email = scanner.nextLine();

        name.matches("[A-Z][a-z]{1,19}");
        date.matches("\\d{2}\\.\\d{2}\\.\\d{4}");
        phone.matches("\\+\\d{11}");
        email.matches("[a-zA-Z0-9\\-\\_\\*\\.]+@[a-z0-9].(com|ru)");
    }
}

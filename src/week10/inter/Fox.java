package week10.inter;

public record Fox(Integer age) implements Animal {
    @Override
    public String getColor() {
        return "Рыжая";
    }

    @Override
    public String getVoice() {
        return "Лиса издает звук: лает";
    }

    @Override
    public void eat(String food) {
        if (food.equals("заяц")) {
            System.out.println("Лиса будет это есть");
        } else {
            System.out.println("Такое лиса не ест");
        }
    }
}

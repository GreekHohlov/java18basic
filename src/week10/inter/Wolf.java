package week10.inter;

public class Wolf implements Animal{

    @Override
    public String getColor() {
        return "Серый";
    }

    @Override
    public String getVoice() {
        return "Воет";
    }

    @Override
    public void eat(String food) {
        System.out.println("Еда для волка - только дикие звери");
    }
}

package week10.inter;

public interface Animal {
    String getColor();
    String getVoice();
    void eat(String food);
}

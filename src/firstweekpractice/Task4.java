package firstweekpractice;

import java.util.Scanner;

/*
   Дана площадь круга, нужно найти диаметр окружности и длину окружности.
    Входные данные:
    91
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        double d, r, l;

        d  = 2 * Math.sqrt(s / Math.PI);
        l = d * Math.PI;
        System.out.println("Диаметр равен: " + d);
        System.out.println("Длина окружности равна: " + l);
Math.random();
    }
}

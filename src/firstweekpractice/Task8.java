package firstweekpractice;

import java.util.Scanner;

/*
    Дано целое число n.
    Выведите следующее за ним четное число.
    При решении задачи нельзя использовать условную инструкцию if и циклы.
    5 -> 6
    10 -> 12
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        System.out.print("Результат " + (n / 2 + 1) * 2);
    }
}

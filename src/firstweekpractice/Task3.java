package firstweekpractice;

import java.util.Scanner;

/*
    Напишите программу, которая получает два числа с плавающей точкой x и y в аргументах
    командной строи и выводит евклидово расстояние от точки (x, y) до точки (0, 0)

    Входные данные
    i = 7 j = 5
 */
public class Task3 {
    public static void main(String[] args) {
        double i, j, d;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите координатные точки x и y: ");
        i = scanner.nextDouble();
        j = scanner.nextDouble();
        d = Math.pow((Math.pow(i, 2) + Math.pow(j, 2) ), 0.5);
        System.out.println("Евклидово расстояник заданных точек равно: " + d);
    }
}
